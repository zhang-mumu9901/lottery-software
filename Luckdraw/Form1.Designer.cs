﻿namespace Luckdraw
{
    partial class Form1
    {
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Button btnBingo;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Timer timer1;
        private Luckdraw.PanelEnhanced panel5;
        private System.Windows.Forms.Panel panBingo;
        private System.Windows.Forms.Label lblBingo;
        private System.Windows.Forms.Label lblCurrentPrize;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel5 = new Luckdraw.PanelEnhanced();
            this.lblCurrentPrizeBigShow = new System.Windows.Forms.Label();
            this.lblCurrnetPrizeDescription = new System.Windows.Forms.Label();
            this.panelRotations = new System.Windows.Forms.Panel();
            this.lblCurrentPrize = new System.Windows.Forms.Label();
            this.panBingo = new System.Windows.Forms.Panel();
            this.lblBingo = new System.Windows.Forms.Label();
            this.btnBingo = new System.Windows.Forms.Button();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.backGroundMusic1 = new Luckdraw.BackGroundMusic();
            this.panel5.SuspendLayout();
            this.panBingo.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Interval = 10;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // panel5
            // 
            this.panel5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel5.BackgroundImage")));
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.lblCurrentPrizeBigShow);
            this.panel5.Controls.Add(this.lblCurrnetPrizeDescription);
            this.panel5.Controls.Add(this.panelRotations);
            this.panel5.Controls.Add(this.lblCurrentPrize);
            this.panel5.Controls.Add(this.panBingo);
            this.panel5.Controls.Add(this.btnBingo);
            this.panel5.Controls.Add(this.lblTitle);
            this.panel5.Controls.Add(this.btnNext);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(939, 522);
            this.panel5.TabIndex = 5;
            // 
            // lblCurrentPrizeBigShow
            // 
            this.lblCurrentPrizeBigShow.AutoSize = true;
            this.lblCurrentPrizeBigShow.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPrizeBigShow.Location = new System.Drawing.Point(26, 154);
            this.lblCurrentPrizeBigShow.Name = "lblCurrentPrizeBigShow";
            this.lblCurrentPrizeBigShow.Size = new System.Drawing.Size(0, 73);
            this.lblCurrentPrizeBigShow.TabIndex = 8;
            // 
            // lblCurrnetPrizeDescription
            // 
            this.lblCurrnetPrizeDescription.AutoSize = true;
            this.lblCurrnetPrizeDescription.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrnetPrizeDescription.ForeColor = System.Drawing.Color.Yellow;
            this.lblCurrnetPrizeDescription.Location = new System.Drawing.Point(26, 260);
            this.lblCurrnetPrizeDescription.Name = "lblCurrnetPrizeDescription";
            this.lblCurrnetPrizeDescription.Size = new System.Drawing.Size(0, 33);
            this.lblCurrnetPrizeDescription.TabIndex = 7;
            // 
            // panelRotations
            // 
            this.panelRotations.Location = new System.Drawing.Point(200, 131);
            this.panelRotations.Name = "panelRotations";
            this.panelRotations.Size = new System.Drawing.Size(555, 264);
            this.panelRotations.TabIndex = 10;
            // 
            // lblCurrentPrize
            // 
            this.lblCurrentPrize.AutoSize = true;
            this.lblCurrentPrize.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentPrize.ForeColor = System.Drawing.SystemColors.Control;
            this.lblCurrentPrize.Location = new System.Drawing.Point(533, 427);
            this.lblCurrentPrize.Name = "lblCurrentPrize";
            this.lblCurrentPrize.Size = new System.Drawing.Size(49, 19);
            this.lblCurrentPrize.TabIndex = 5;
            this.lblCurrentPrize.Text = "label1";
            // 
            // panBingo
            // 
            this.panBingo.AutoScroll = true;
            this.panBingo.Controls.Add(this.lblBingo);
            this.panBingo.Location = new System.Drawing.Point(744, 131);
            this.panBingo.Name = "panBingo";
            this.panBingo.Size = new System.Drawing.Size(191, 264);
            this.panBingo.TabIndex = 3;
            // 
            // lblBingo
            // 
            this.lblBingo.AutoSize = true;
            this.lblBingo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBingo.ForeColor = System.Drawing.Color.White;
            this.lblBingo.Location = new System.Drawing.Point(14, 9);
            this.lblBingo.Name = "lblBingo";
            this.lblBingo.Size = new System.Drawing.Size(0, 20);
            this.lblBingo.TabIndex = 0;
            // 
            // btnBingo
            // 
            this.btnBingo.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnBingo.BackgroundImage")));
            this.btnBingo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnBingo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBingo.ForeColor = System.Drawing.Color.Yellow;
            this.btnBingo.Location = new System.Drawing.Point(425, 424);
            this.btnBingo.Name = "btnBingo";
            this.btnBingo.Size = new System.Drawing.Size(102, 28);
            this.btnBingo.TabIndex = 2;
            this.btnBingo.Text = "开始";
            this.btnBingo.UseVisualStyleBackColor = false;
            this.btnBingo.Click += new System.EventHandler(this.button3_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.SystemColors.Window;
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.Yellow;
            this.lblTitle.Location = new System.Drawing.Point(343, 41);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(313, 55);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "LUCK DRAW";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.SystemColors.Control;
            this.btnNext.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnNext.BackgroundImage")));
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.ForeColor = System.Drawing.Color.Yellow;
            this.btnNext.Location = new System.Drawing.Point(401, 424);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(106, 29);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "抽取下一奖项";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.button1_Click);
            // 
            // backGroundMusic1
            // 
            this.backGroundMusic1.ExitCode = 0;
            this.backGroundMusic1.ServiceName = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(939, 522);
            this.Controls.Add(this.panel5);
            this.ForeColor = System.Drawing.SystemColors.Control;
            this.KeyPreview = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panBingo.ResumeLayout(false);
            this.panBingo.PerformLayout();
            this.ResumeLayout(false);

        }


        #endregion
        private System.Windows.Forms.Panel panelRotations;
        private System.Windows.Forms.Label lblCurrentPrizeBigShow;
        private System.Windows.Forms.Label lblCurrnetPrizeDescription;
        private BackGroundMusic backGroundMusic1;
    }
}

